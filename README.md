# `mlogpy`
## Description
Compiles python to mlog, the language of the logic processors in [Mindustry](https://mindustrygame.github.io).

## API
set:
```py
a = 4
```

op:
```py
a = 4*3
```

write:
```py
cell1[2] = 4
```

read:
```py
a = cell1[5]
```

drawing:
```py
draw.clear(0, 0, 0)
draw.color(255, 0, 0)
draw.rect(0, 0, 40, 40)
draw.flush(display1)
```

printing:
```py
print(x)
print.flush(message1)
```

getlink:
```py
ml = link(0)
```

control:
```py
control.enabled(switch1, False)
```

constants:
```py
m.copper # same as @copper in mlog
```

unit bind:
```py
unit.bind(m.poly)
```

unit control:
```py
unit.move(10, 20)
unit.stop()
unit.within(100, 100, 10, x)
if x: ...
```

unit locate:
```py
unit.locate(ore, m.titanium, outx, outy, found)
```
