import ast
from ast import AST
import io
from copy import copy
from typing import Optional, List, Sequence


class Jump(AST):
    _fields = ("label", "cond")

    def __init__(self, label: str, cond: Optional[AST]):
        self.label = label
        self.cond = cond


class Label(AST):
    _fields = ("name",)

    def __init__(self, name: str):
        self.name = name


class JumpTransformer(ast.NodeTransformer):
    def __init__(self):
        self.label_count = 0

    def new_label(self):
        self.label_count += 1
        return f"__jt_label{self.label_count}"

    def visit_If(self, node: ast.If) -> Sequence[AST]:
        label1 = self.new_label()
        new_nodes: List[AST] = [Jump(label1, ast.UnaryOp(ast.Not(), node.test))]
        new_nodes.extend(self.visit_list(node.body))
        if node.orelse:
            label2 = self.new_label()
            new_nodes.append(Jump(label2, None))
            new_nodes.append(Label(label1))
            new_nodes.extend(self.visit_list(node.orelse))
            new_nodes.append(Label(label2))
        else:
            new_nodes.append(Label(label1))
        return new_nodes

    def visit_While(self, node: ast.While) -> Sequence[AST]:
        start_label = self.new_label()
        end_label = self.new_label()
        new_nodes = [
            Label(start_label),
            Jump(end_label, ast.UnaryOp(ast.Not(), node.test)),
        ]
        new_nodes.extend(self.visit_list(node.body))
        new_nodes.append(Jump(start_label, None))
        new_nodes.append(Label(end_label))
        return new_nodes

    def visit_list(self, lst: Sequence[AST]) -> Sequence[AST]:
        r = []
        for n in lst:
            nn = self.visit(n)
            if isinstance(nn, AST):
                r.append(nn)
            else:
                r.extend(nn)
        return r


class NotCompareTransformer(ast.NodeTransformer):
    def visit_UnaryOp(self, node: ast.UnaryOp) -> AST:
        if isinstance(node.op, ast.Not):
            opposite = {
                ast.Eq: ast.NotEq,
                ast.NotEq: ast.Eq,
                ast.Lt: ast.GtE,
                ast.LtE: ast.Gt,
                ast.Gt: ast.LtE,
                ast.GtE: ast.Lt,
            }
            ope = node.operand
            if (
                isinstance(ope, ast.Compare)
                and len(ope.ops) == 1
                and (o := opposite.get(type(ope.ops[0]))) is not None
            ):
                n = copy(ope)
                n.ops[0] = o()
                return self.visit(n)
            if isinstance(ope, ast.UnaryOp) and isinstance(ope.op, ast.Not):
                return self.visit(ope.operand)
        return self.generic_visit(node)


class MlogVisitor(ast.NodeVisitor):
    def __init__(self, output: io.TextIOBase):
        self.output = output
        self.expr_target = "__tmp0"
        self.tmp_count = 0
        self.label_count = 0
        self.lines = 0

    def outputln(self, s: str) -> None:
        self.output.write(s + "\n")
        self.lines += 1

    def visit_Assign(self, node: ast.Assign) -> None:
        if not node.targets:
            return

        t = node.targets[0]
        if isinstance(t, ast.Name):
            first_target = t.id
            simple = self.get_simple_expr(node.value)
            if simple is not None:
                self.outputln(f"set {first_target} {simple}")
            else:
                self.visit_expr(node.value, first_target)
        elif isinstance(t, ast.Subscript):
            v = self.visit_expr(node.value)
            c = self.visit_expr(t.value)
            i = self.visit_expr(t.slice)
            self.outputln(f"write {v} {c} {i}")
        else:
            print(f"Line {t.lineno}, column {t.col_offset}:")
            print(f"  Unsupported assignment to {ast.dump(t)}")
            raise NotImplementedError()

        next_assign = copy(node)
        next_assign.targets = next_assign.targets[1:]
        next_assign.value = node.targets[0]
        self.visit_Assign(next_assign)

    def visit_AugAssign(self, node: ast.AugAssign) -> None:
        op = {
            ast.Add: "add",
            ast.Sub: "sub",
            ast.Mult: "mul",
            ast.Div: "div",
            ast.FloorDiv: "idiv",
            ast.Mod: "mod",
            ast.Pow: "pow",
            ast.LShift: "shl",
            ast.RShift: "shr",
            ast.BitOr: "or",
            ast.BitXor: "xor",
            ast.BitAnd: "and",
        }.get(type(node.op))
        if op is None:
            print(f"Line {node.lineno}, column {node.col_offset}:")
            print(f"  Unsupported binary operation: {node.op}")
            raise NotImplementedError()
        target = self.visit_expr(node.target)
        value = self.visit_expr(node.value)
        self.outputln(f"op {op} {target} {target} {value}")

    def visit_Pass(self, node: ast.Pass) -> None:
        self.outputln("noop")

    def visit_Expr(self, expr: ast.Expr) -> None:
        self.visit_expr(expr.value)

    # def visit_If(self, node):
    #     label1 = self.new_label()
    #     self.jump_if_not(label1, node.test)
    #     for n in node.body: self.visit(n)
    #     if node.orelse:
    #         label2 = self.new_label()
    #         self.jump_if(label2, True)
    #         self.outputln(f"label {label1}")
    #         for n in node.orelse: self.visit(n)
    #         self.outputln(f"label {label2}")
    #     else:
    #         self.outputln(f"label {label1}")

    # def visit_While(self, node):
    #     start_label = self.new_label()
    #     end_label = self.new_label()
    #     self.outputln(f"label {start_label}")
    #     self.jump_if_not(end_label, node.test)
    #     for n in node.body: self.visit(n)
    #     self.jump_if(start_label, True)
    #     self.outputln(f"label {end_label}")

    def get_simple_expr(self, node: ast.expr) -> Optional[str]:
        if isinstance(node, ast.Constant):
            return self.constant_repr(node)
        if isinstance(node, ast.Name):
            if node.id == "self":
                return "@this"
            return node.id
        # if (
        #     isinstance(node, ast.Attribute)
        #     and isinstance(node.value, ast.Name)
        #     and node.value.id == "self"
        # ):
        #     return {
        #         "unit": "@unit",
        #         "x": " @thisx",
        #         "y": " @thisy",
        #     }.get(node.attr)
        if (
            isinstance(node, ast.Attribute)
            and isinstance(node.value, ast.Name)
            and node.value.id == "m"
        ):
            return "@" + node.attr
        return None

    def visit_expr(self, node: ast.expr, target: str = None) -> str:
        simple = self.get_simple_expr(node)
        if simple is not None:
            return simple

        prev = self.expr_target
        cur = self.new_tmp() if target is None else target
        self.expr_target = cur
        self.visit(node)
        self.expr_target = prev
        return cur

    def visit_Call(self, call):
        class CallError(ValueError):
            pass

        if isinstance(call.func, ast.Attribute) and isinstance(
            call.func.value, ast.Name
        ):
            ns = call.func.value.id
            f = call.func.attr
            if ns == "draw":
                if f == "flush":
                    self.outputln(f"drawflush {self.visit_expr(call.args[0])}")
                    return
                line = f"draw {f} "
                line += " ".join(map(self.visit_expr, call.args))
                self.outputln(line)
                return
            if ns == "print" and f == "flush":
                if len(call.args) != 1:
                    raise CallError()
                self.outputln(f"printflush {self.visit_expr(call.args[0])}")
                return
            if ns == "control":
                line = f"control {f} "
                line += " ".join(map(self.visit_expr, call.args))
                self.outputln(line)
                return
            if ns == "unit":
                if f in {
                    "stop",
                    "move",
                    "approach",
                    "boost",
                    "pathfind",
                    "target",
                    "targetp",
                    "itemDrop",
                    "itemTake",
                    "payDrop",
                    "payTake",
                    "mine",
                    "flag",
                    "build",
                    "getBlock",
                    "within",
                }:
                    line = f"ucontrol {f} "
                    line += " ".join(map(self.visit_expr, call.args))
                    self.outputln(line)
                    return
                if f == "locate":
                    line = "ulocate "
                    line += " ".join(map(self.visit_expr, call.args))
                    self.outputln(line)
                    return
                if f == "bind":
                    if len(call.args) != 1:
                        raise CallError()
                    unit = self.get_simple_expr(call.args[0])
                    if unit is not None:
                        self.outputln(f"ubind {unit}")
                        return
        elif isinstance(call.func, ast.Name):
            f = call.func.id
            if f == "print":
                if len(call.args) != 1:
                    raise CallError()
                self.outputln(f"print {self.visit_expr(call.args[0])}")
                return
            if f == "link":
                if len(call.args) != 1:
                    raise CallError()
                i = self.visit_expr(call.args[0])
                self.outputln(f"getlink {self.expr_target} {i}")
                return

        print(f"Line {call.lineno}, column {call.col_offset}:")
        print(f"  Invalid function call: {ast.dump(call)}")
        raise ValueError()

    def visit_Subscript(self, node):
        assert isinstance(node.ctx, ast.Load)

        v = self.visit_expr(node.value)
        i = self.visit_expr(node.slice)

        self.outputln(f"read {self.expr_target} {v} {i}")

    def visit_Constant(self, node):
        self.outputln(f"set {self.expr_target} {self.constant_repr(node)}")

    def visit_BinOp(self, binop):
        op = {
            ast.Add: "add",
            ast.Sub: "sub",
            ast.Mult: "mul",
            ast.Div: "div",
            ast.FloorDiv: "idiv",
            ast.Mod: "mod",
            ast.Pow: "pow",
            ast.LShift: "shl",
            ast.RShift: "shr",
            ast.BitOr: "or",
            ast.BitXor: "xor",
            ast.BitAnd: "and",
        }.get(type(binop.op))
        if op is None:
            print(f"Line {binop.lineno}, column {binop.col_offset}:")
            print(f"  Unsupported binary operation: {binop.op}")
            raise NotImplementedError()
        left = self.visit_expr(binop.left)
        right = self.visit_expr(binop.right)
        self.outputln(f"op {op} {self.expr_target} {left} {right}")

    def visit_Compare(self, compare):
        if len(compare.ops) != 1 or len(compare.comparators) != 1:
            print(f"Line {compare.lineno}, column {compare.col_offset}:")
            print("  Only single comparisons are supported")
            raise NotImplementedError()

        op = {
            ast.Eq: "equal",
            ast.NotEq: "notEqual",
            ast.Lt: "lessThan",
            ast.LtE: "lessThanEq",
            ast.Gt: "greaterThan",
            ast.GtE: "greaterThanEq",
        }.get(type(compare.ops[0]))
        if op is None:
            print(f"Line {compare.lineno}, column {compare.col_offset}:")
            print(f"  Unsupported comparison: {compare.ops[0]}")
            raise NotImplementedError()
        left = self.visit_expr(compare.left)
        right = self.visit_expr(compare.comparators[0])
        self.outputln(f"op {op} {self.expr_target} {left} {right}")

    def visit_BoolOp(self, boolop):
        op = {
            ast.And: "land",
            ast.Or: "or",
        }.get(type(boolop.op))
        self.outputln(
            f"op {op} {self.expr_target} {self.visit_expr(boolop.values[0])} {self.visit_expr(boolop.values[1])}"
        )
        for i in range(2, len(boolop.values)):
            self.outputln(
                f"op {op} {self.expr_target} {self.expr_target} {self.visit_expr(boolop.values[i])}"
            )

    def visit_Name(self, name):
        self.outputln(f"set {self.expr_target} {name.id}")

    def visit_Module(self, mod):
        for node in mod.body:
            self.visit(node)

    def visit_UnaryOp(self, node):
        if isinstance(node.op, ast.UAdd):
            return
        o = self.visit_expr(node.operand)

        if isinstance(node.op, ast.USub):
            self.outputln(f"op mul {self.expr_target} {o} -1")
        elif isinstance(node.op, ast.Invert):
            self.outputln(f"op not {self.expr_target} {o}")
        elif isinstance(node.op, ast.Not):
            self.outputln(f"op equal {self.expr_target} {o} false")

    def visit_Attribute(self, node):
        v = self.visit_expr(node.value)
        self.outputln(f"sensor {self.expr_target} {v} @{node.attr}")

    def visit_Jump(self, jump):
        if jump.cond is None:
            self.outputln(f"jump {jump.label} always")
        elif isinstance(jump.cond, ast.Compare):
            compare = jump.cond
            if len(compare.ops) != 1 or len(compare.comparators) != 1:
                print(f"Line {compare.lineno}, column {compare.col_offset}:")
                print("  Only single comparisons are supported")
                raise NotImplementedError()
            op = {
                ast.Eq: "equal",
                ast.NotEq: "notEqual",
                ast.Lt: "lessThan",
                ast.LtE: "lessThanEq",
                ast.Gt: "greaterThan",
                ast.GtE: "greaterThanEq",
            }.get(type(compare.ops[0]))
            if op is None:
                print(f"Line {compare.lineno}, column {compare.col_offset}:")
                print(f"  Unsupported comparison: {compare.ops[0]}")
                raise NotImplementedError()
            left = self.visit_expr(compare.left)
            right = self.visit_expr(compare.comparators[0])
            self.outputln(f"jump {jump.label} {op} {left} {right}")
        elif isinstance(jump.cond, ast.UnaryOp) and isinstance(jump.cond.op, ast.Not):
            r = self.visit_expr(jump.cond.operand)
            self.outputln(f"jump {jump.label} equal {r} false")
        else:
            r = self.visit_expr(jump.cond)
            self.outputln(f"jump {jump.label} not {r} false")

    def visit_Label(self, label):
        self.outputln(f"label {label.name}")

    def new_tmp(self):
        self.tmp_count += 1
        return f"__tmp{self.tmp_count}"

    def new_label(self):
        self.label_count += 1
        return f"__label{self.label_count}"

    def constant_repr(self, constant):
        c = constant.value
        if c is True:
            return "true"
        if c is False:
            return "false"
        if c is None:
            return "null"
        if isinstance(c, float) or isinstance(c, int):
            return str(c)
        if isinstance(c, str):
            return f'"{c}"'
        print(f"Line {constant.lineno}, column {constant.col_offset}:")
        print(f"  Unsupported constant type {type(c)}")
        raise NotImplementedError()

    def generic_visit(self, node):
        print(f"Line {node.lineno}, column {node.col_offset}:")
        print("  Unsupported AST node:")
        print(ast.dump(node, indent=2))
        raise NotImplementedError()


def resolve_labels(inp, out):
    labels = {}
    lineno = 0
    lines = inp.readlines()
    for line in lines:
        w = line.split(maxsplit=2)
        if w[0] == "label":
            if w[1] in labels:
                print("Multiple labels with the same name")
                raise ValueError()
            labels[w[1]] = lineno
        else:
            lineno += 1

    for line in lines:
        w = line.split(maxsplit=2)
        if w[0] == "jump":
            out.write("jump ")
            dest = labels[w[1]]
            if dest == lineno:
                dest = 0
            out.write(str(dest))
            out.write(" ")
            out.write(w[2])
        elif w[0] != "label":
            out.write(line)


def compile_mpy(node: ast.Module, out: io.TextIOBase) -> None:
    node = JumpTransformer().visit(node)
    node = NotCompareTransformer().visit(node)
    buf = io.StringIO()
    MlogVisitor(buf).visit(node)
    buf.seek(0)
    resolve_labels(buf, out)
