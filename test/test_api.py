import pytest
from test.helper import comp_test


def test_write():
    comp_test(
        "cell1[0] = 3",
        "write 3 cell1 0\n"
    )

def test_read():
    comp_test(
        "x = cell1[3]",
        "read x cell1 3\n"
    )

def test_multi_assign_write():
    comp_test(
        "a = cell1[2] = 3",

        "set a 3\n"
        "write a cell1 2\n"
    )

def test_aug_assign():
    comp_test(
        "a *= 3",
        "op mul a a 3\n"
    )

@pytest.mark.skip(reason="unimplemented")  # TODO
def test_aug_assign_cell():
    comp_test(
        "cell1[4] += 2",

        "read __tmp1 cell1 4\n"
        "op add __tmp1 __tmp1 2\n"
        "write __tmp1 cell1 4\n"
    )

def test_draw():
    comp_test(
        "draw.clear(0, 0, 0)\n"
        "draw.color(255, 0, 0)\n"
        "draw.rect(0, 0, 40, 40)\n"
        "draw.flush(display1)\n",

        "draw clear 0 0 0\n"
        "draw color 255 0 0\n"
        "draw rect 0 0 40 40\n"
        "drawflush display1\n"
    )

def test_print():
    comp_test(
        "print(x)\n"
        "print.flush(message1)\n",

        "print x\n"
        "printflush message1\n"
    )

def test_getlink():
    comp_test(
        "ml = link(0)",
        "getlink ml 0\n"
    )

def test_control():
    comp_test(
        "control.enabled(switch1, False)",
        "control enabled switch1 false\n"
    )

def test_mlog_constants():
    comp_test(
        "x = m.copper",
        "set x @copper\n"
    )

def test_bind():
    comp_test(
        "unit.bind(m.poly)",
        "ubind @poly\n"
    )

def test_unit_control():
    comp_test(
        "unit.move(10, 20)\n"
        "unit.stop()\n"
        "unit.within(100, 100, 10, x)\n",

        "ucontrol move 10 20\n"
        "ucontrol stop \n"  # trailing space
        "ucontrol within 100 100 10 x\n"
    )

def test_unit_locate():
    comp_test(
        "unit.locate(ore, m.titanium, outx, outy, found)",
        "ulocate ore @titanium outx outy found\n"
    )
