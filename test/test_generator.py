from test.helper import comp_test


def test_assign():
    comp_test(
        "a = 3",
        "set a 3\n"
    )

def test_multi_assign():
    comp_test(
        "a = b = 3",

        "set a 3\n"
        "set b a\n"
    )

def test_pass():
    comp_test(
        "pass",
        "noop\n"
    )

def test_math():
    comp_test(
        "x = 3*4/2 + (4%3//5)**(2 ^ 6)",

        "op mul __tmp2 3 4\n"
        "op div __tmp1 __tmp2 2\n"
        "op mod __tmp5 4 3\n"
        "op idiv __tmp4 __tmp5 5\n"
        "op xor __tmp6 2 6\n"
        "op pow __tmp3 __tmp4 __tmp6\n"
        "op add x __tmp1 __tmp3\n"
    )