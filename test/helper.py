import mpy

def comp_test(src, expected):
    import io
    import ast
    buf = io.StringIO()
    mpy.compile_mpy(ast.parse(src, "<string>"), buf)
    mlog = buf.getvalue()
    assert mlog == expected
